const container = document.getElementById('app')
const sizeSelector = document.getElementById('size')

const imgSrc = 'https://cs.pikabu.ru/images/jobseeker/logo2.png'

class Element {
    constructor(index, size, count) {
        this.x = index % count
        this.y = Math.floor(index / count)
        this.index = index
        this.size = size
        this.backgroundX = this.x * this.size
        this.backgroundY = this.y * this.size

    }
}

class App {
    _size = 80
    count = 5
    elements = []
    currentEmptyIndex = (this.count ** 2) - 1
    currentEmptyElement = null
    constructor(container, sizeSelector) {
        this.container = container
        this.sizeSelector = sizeSelector
    }

    get size () {
        return this._size
    }

    set size (value) {
        this._size = Math.floor(400 / value)
        this.count = value
        this.currentEmptyIndex = (this.count ** 2) - 1

        this.start()
    }

    start() {
        this.createElements()

        this.container.innerHTML = ''

        this.container.append(this.insertElements())

        this.shuffleElements()

        setTimeout(() => {
            this.container.innerHTML = ''
            this.container.append(this.insertElements())
        }, 1000)
    }

    startEventListeners () {
        this.container.addEventListener('click', (event) => {
            let currentElement = event.target
            let currentElementIndex = parseInt(currentElement.dataset.index)
            let currentEmptyIndex = this.currentEmptyIndex
            if (currentElementIndex === currentEmptyIndex) {
                return false
            }
            if ((currentElementIndex === currentEmptyIndex - 1 ||
                currentElementIndex === currentEmptyIndex + 1  ||
                currentElementIndex === currentEmptyIndex - this.count  ||
                currentElementIndex === currentEmptyIndex + this.count)) {
                if (
                    (currentElementIndex % this.count === this.count - 1 && currentEmptyIndex % this.count === 0) ||
                    (currentElementIndex % this.count === 0 && currentEmptyIndex % this.count === this.count - 1)
                ) {
                    this.disableTransitionAnimate(currentElement)
                    return false
                }
                let direction = null
                if (currentElementIndex === currentEmptyIndex - 1) {
                    direction = 'right'
                }
                else if (currentElementIndex === currentEmptyIndex + 1) {
                    direction = 'left'
                }
                else if (currentElementIndex === currentEmptyIndex - this.count) {
                    direction = 'bottom'
                }
                else if (currentElementIndex === currentEmptyIndex + this.count) {
                    direction = 'top'
                }
                let currentEmptyElement = this.currentEmptyElement
                let siblingElement = currentEmptyElement.previousElementSibling
                if (siblingElement === currentElement) {
                    siblingElement = currentEmptyElement
                }
        
                currentEmptyElement.dataset.index = this.currentEmptyIndex = currentElementIndex
        
                container.replaceChild(currentEmptyElement, currentElement)
                currentElement.dataset.index = currentEmptyIndex
                
                if (siblingElement) {
                    siblingElement.after(currentElement)
                } else {
                    currentEmptyElement.before(currentElement)
                }
                this.transitionAnimate(currentElement, direction)
            } else {
                this.disableTransitionAnimate(currentElement)
            }
        })
        
        this.sizeSelector.addEventListener('change', (event) => {
            this.size = parseInt(event.target.value)
        })
    }

    createElements() {
        this.elements = []
        const limit = this.count ** 2
        for (let i = 0; i < limit; i++) {
            this.elements.push(new Element(i, this.size, this.count))
        }
    }

    shuffleElements() {
        for (let i = this.elements.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1))
            let temporary = this.elements[i]
            let indexI = this.elements[i].index
            let indexJ = this.elements[j].index
            this.elements[i] = this.elements[j]
            this.elements[i].index = indexI
            this.elements[j] = temporary
            this.elements[j].index = indexJ
        }
    }

    insertElements() {
        const fragment = new DocumentFragment();

        this.elements.forEach((el, index) => {
            let card = document.createElement('div')

            card.style.width = `${el.size}px`
            card.style.height = `${el.size}px`

            card.dataset.index = el.index

            if (index !== this.currentEmptyIndex) {
                card.style.backgroundImage = `url(${imgSrc})`
                card.style.backgroundPosition = `-${el.backgroundX}px -${el.backgroundY}px`
            } else {
                card.className = 'empty'
                this.currentEmptyElement = card
            }

            fragment.append(card)
        })

        return fragment
    }

    disableTransitionAnimate(element) {
        if (!element) {
            return false
        }

        element.style.right = ''
        element.style.top = ''
        element.style.left = ''
        element.style.bottom = ''

        const start = Date.now()
        let direction = 0
        let px = 5
        let animation = setInterval(() => {
            const time = Date.now()
            const diff = time - start
            
            px = (600 - diff) / 100
            if (diff < 600) {

                if (direction === 0) {
                    element.style.left = `${px}px`
                    element.style.right = ``
                    direction = 1
                } else {
                    element.style.right = `${px}px`
                    element.style.left = ``
                    direction = 0
                }
            } else {
                element.style.left = '0px'
                clearInterval(animation)
            }
        }, 80)
    }

    transitionAnimate(element, direction) {
        if (!direction || !element) {
            return false
        }
        element.style.right = ''
        element.style.top = ''
        element.style.left = ''
        element.style.bottom = ''

        element.style[direction] = `${this.size}px`
        let timer = setTimeout(() => {
            element.style[direction] = '0px'
            clearTimeout(timer)
        }, 0)
    }
}

const app = new App(container, sizeSelector)

app.start()

app.startEventListeners()
